"""Manage fee and slippage estimation."""
from .fee_table import fee_table


class Fee:

    """A simple research-based fee calculator.

    Parameters
    ----------
    price: Int
        Target price for the trade.

    Attributes
    ----------
    price: Int
        Target price for the trade.

    Methods
    -------
    estimate_slippage(hlc_0, hlc_1)
        Estimate slippage according to Abdi and Ranaldo (2017).

        Returns tuple of $, %.

        >>> Fee.estimate_slippage([100, 101, 102], [101, 102, 103])
        (4.0, 0.038834951456310676)

    estimate_comms(allocation, buy=True)
        Estimate IBKR commissions based off their table, allocation
        size, and whether or not the trade is a purchase or sale.

        Returns tuple of $, %, str describing optimal
        commission structure for trade.

        >>> Fee.estimate_comms(500)
        (0.354, 0.000708, 'tiered')

    estimate_fees(allocation, hlc_0, hlc_1,
                  buy=True, multiplier=1, pct=True)
        Estimate all hidden fees for the trade; combination
        of slippage and commission estimation.

        Returns $ or % depending on pct parameter specification.
        The default is %.

        >>> Fee.estimate_fees(500, [100, 101, 102], [101, 102, 103])
        0.03954295145631068

    """

    def __init__(self, price):
        """Initialize object to estimate fees.

        Parameters
        ----------
        price : float
            Target price for trade.

        """
        self.price = price

    @staticmethod
    def estimate_slippage(hlc_0, hlc_1):
        """Estimate slippage according to Abdi and Ranaldo (2017).

        Parameters
        ----------
        hlc_0 : tuple
            high, low, close.
        hlc_1 : tuple
            high, low, close.

        Returns
        -------
        tuple
            Exp. Slippage in USD, Slippage as % Trade Value.

        """
        h_0, l_0, c_0 = hlc_0
        h_1, l_1, c_1 = hlc_1

        n_0 = (h_0 + l_0) / 2
        n_1 = (h_1 + l_1) / 2

        s = (c_1 - (n_1 + n_0) / 2) ** 2

        s_pct = s / c_1

        return s, s_pct

    def estimate_comms(self, allocation, buy=True):
        """Estimate IBKR commissions based off their table.

        Parameters
        ----------
        allocation : float
            USD assigned to the trade.
        buy : bool, optional
            Whether long or short. The default is True.

        Returns
        -------
        tuple
            (price, price_pct, price_type)
            price : float
                Price in USD.
            price_pct : float
                Price as % of TTV.
            price_type : str
                Tiered or Fixed comm structure selected.

        """
        shares = allocation // self.price
        ttv = shares * self.price

        tiered_table = fee_table.TIERED[
            shares > fee_table.TIERED.index
            ].iloc[-1]

        price_tiered = tiered_table.per_share * shares
        if price_tiered < tiered_table.minimum:
            price_tiered = tiered_table.minimum
        elif (price_tiered / ttv) > tiered_table.max_pct:
            price_tiered = tiered_table.max_pct * ttv

        limit = fee_table.FIXED.max_pct.iloc[0]
        price_fixed = fee_table.FIXED.per_share.iloc[0] * shares
        if price_fixed < fee_table.FIXED.minimum.iloc[0]:
            price_fixed = fee_table.FIXED.minimum.iloc[0]
        elif (price_fixed / ttv) > limit:
            price_fixed = limit * ttv
        if not buy:
            v0 = price_tiered * fee_table.NYSE_FEE
            v1 = price_tiered * fee_table.FINRA_FEE
            price_tiered += v0 + v1

        clearing_fees = shares * fee_table.CLEARING_FEE_PS
        if clearing_fees > 0.005 * ttv:
            clearing_fees = 0.005 * ttv

        price_tiered += clearing_fees
        price_tiered += fee_table.EXCHANGE_FEE

        if price_tiered < price_fixed:
            price = price_tiered
            price_type = 'tiered'
        else:
            price = price_fixed
            price_type = 'fixed'

        if not buy:
            price += (fee_table.FINRA_TRADING_FEE * shares)
            price += (fee_table.TRANSACTION_FEE * ttv)

        return price, price / ttv, price_type

    def estimate_fees(self, allocation, hlc_0, hlc_1, buy=True, multiplier=1, pct=True): # noqa
        """Estimate all hidden fees for the trade.

        Parameters
        ----------
        allocation : float
            USD assigned to the trade.
        hlc_0 : tuple
            high, low, close.
        hlc_1 : tuple
            high, low, close.
        buy : bool, optional
            Whether long or short. The default is True.
        multiplier : float, optional
            Increase or decrease estimate arbitrarily.

        Returns
        -------
        float
            Total exp. fees in USD.

        """
        comms = self.estimate_comms(allocation, buy=buy)
        slippage = self.estimate_slippage(hlc_0, hlc_1)
        idx = 1 if pct else 0
        return (comms[idx] + slippage[idx]) * multiplier
