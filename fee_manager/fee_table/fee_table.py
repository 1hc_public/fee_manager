"""IBKR Fee Table."""
import pandas as pd

FIXED = pd.DataFrame(
    [[0.0075, 1, 0.01]],
    columns=[
        'per_share',
        'minimum',
        'max_pct'
        ]
    )

TIERED = pd.DataFrame(
    [
        [0.0035, 0.35, 0.01],
        [0.002, 0.35, 0.01],
        [0.0015, 0.35, 0.01],
        [0.001, 0.35, 0.01],
        [0.0005, 0.35, 0.01],
        ],
    index=[
        0,
        3e5,
        3e6,
        2e8,
        1e9
        ],
    columns=[
        'per_share',
        'minimum',
        'max_pct'
        ]
    )

EXCHANGE_FEE = 0.003

# Maximum %0.5 of trade
CLEARING_FEE_PS = 0.0002

# * total trade value, only sell orders
TRANSACTION_FEE = 0.0000221

# * Tiered Commission
NYSE_FEE = 0.000175

# * Tiered Commission
FINRA_FEE = 0.00056

# * Volume
FINRA_TRADING_FEE = 0.000119
