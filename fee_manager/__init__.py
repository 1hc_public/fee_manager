"""IBKR Fee Estimator."""
from .fee_manager import Fee # noqa

__version__ = '1.1.8'
