"""Fee Manager setup."""
from setuptools import setup, find_packages

requirements_path = 'requirements.txt'
with open(requirements_path) as f:
    requirements = f.read().splitlines()

setup(name='fee_manager',
      version='1.1.8',
      description='Simplifies operations around IBKR fees.',
      url='https://gitlab.com/1hc_public/fee_manager',
      author='1 Howard Capital LLC',
      author_email='support@1howardcapital.com',
      license="Proprietary",
      classifiers=[
          'License :: Other/Proprietary License',
          ],
      packages=find_packages('.',
                             exclude=['tests',
                                      'tests.*',
                                      '*.tests',
                                      '*.tests.*']),
      install_requires=requirements,
      zip_safe=False)
