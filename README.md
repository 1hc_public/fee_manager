# fee_manager package

## Subpackages


* fee_manager.fee_table package


    * Submodules


    * fee_manager.fee_table.fee_table module


    * Module contents


## Submodules

## fee_manager.fee_manager module

Manage fee and slippage estimation.


### class fee_manager.fee_manager.Fee(price)
Bases: `object`

Simple, research-based fee calculator.


* **Parameters**

    **price** (*Int*) – Target price for the trade.



#### price()
Target price for the trade.


* **Type**

    Int



#### estimate_slippage(hlc_0, hlc_1)
Estimate slippage according to Abdi and Ranaldo (2017).

Returns tuple of $, %.

```python
>>> Fee.estimate_slippage([100, 101, 102], [101, 102, 103])
(4.0, 0.038834951456310676)
```


#### estimate_comms(allocation, buy=True)
Estimate IBKR commissions based off their table, allocation
size, and whether or not the trade is a purchase or sale.

Returns tuple of $, %, str describing optimal
commission structure for trade.

```python
>>> Fee.estimate_comms(500)
(0.354, 0.000708, 'tiered')
```


### estimate_fees(allocation, hlc_0, hlc_1,()
> buy=True, multiplier=1, pct=True)

Estimate all hidden fees for the trade; combination
of slippage and commission estimation.

Returns $ or % depending on pct parameter specification.
The default is %.

```python
>>> Fee.estimate_fees(500, [100, 101, 102], [101, 102, 103])
0.03954295145631068
```


#### estimate_comms(allocation, buy=True)
Estimate IBKR commissions based off their table.


* **Parameters**

    
    * **allocation** (*float*) – USD assigned to the trade.


    * **buy** (*bool**, **optional*) – Whether long or short. The default is True.



* **Returns**

    
    * **price** (*float*) – Price in USD.


    * **price_pct** (*float*) – Price as % of TTV.


    * **price_type** (*str*) – Tiered or Fixed comm structure selected.




#### estimate_fees(allocation, hlc_0, hlc_1, buy=True, multiplier=1, pct=True)
Estimate all hidden fees for the trade.


* **Parameters**

    
    * **allocation** (*float*) – USD assigned to the trade.


    * **hlc_0** (*tuple*) – high, low, close.


    * **hlc_1** (*tuple*) – high, low, close.


    * **buy** (*bool**, **optional*) – Whether long or short. The default is True.


    * **multiplier** (*float**, **optional*) – Increase or decrease estimate arbitrarily.



* **Returns**

    Total exp. fees in USD.



* **Return type**

    float



#### estimate_slippage(hlc_0, hlc_1)
Estimate slippage according to Abdi and Ranaldo (2017).


* **Parameters**

    
    * **hlc_0** (*tuple*) – high, low, close.


    * **hlc_1** (*tuple*) – high, low, close.



* **Returns**

    


* **Return type**

    Exp. Slippage in USD, Slippage as % Trade Value.


## Module contents

IBKR Fee Estimator.
