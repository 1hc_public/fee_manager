"""Top level simple unit tests."""
import unittest

from fee_manager.fee_table import fee_table

# --------------------------------------------------------------------------- #


class TestFeeTable(unittest.TestCase):

    """Basic unit tests."""

    def test_fixed(self):
        """Make sure the something is there."""
        self.assertIsNotNone(fee_table.FIXED)

    def test_tiered(self):
        """Make sure the something is there."""
        self.assertIsNotNone(fee_table.TIERED)

    def test_exchange_fee(self):
        """Make sure the something is there."""
        self.assertIsInstance(fee_table.EXCHANGE_FEE, float)

    def test_clearing_fee(self):
        """Make sure the something is there."""
        self.assertIsInstance(fee_table.CLEARING_FEE_PS, float)

    def test_transaction_fee(self):
        """Make sure the something is there."""
        self.assertIsInstance(fee_table.TRANSACTION_FEE, float)
