"""Top level simple unit tests."""
import unittest

from fee_manager import Fee

# --------------------------------------------------------------------------- #


class TestFee(unittest.TestCase):

    """Basic unit tests."""

    def test_estimate_slippage(self):
        """Make sure this works."""
        hlc_0 = [100, 101, 102]
        hlc_1 = [101, 102, 103]
        target = (4.0, 0.038834951456310676)
        result = Fee.estimate_slippage(hlc_0, hlc_1)
        self.assertEquals(target, result)

    def test_estimate_comms(self):
        """Make sure this works."""
        allocation = 500
        target = (0.354, 0.000708, 'tiered')
        result = Fee(100).estimate_comms(allocation)
        self.assertEquals(target, result)

    def test_estimate_fees(self):
        """Make sure this works."""
        allocation = 500
        hlc_0 = [100, 101, 102]
        hlc_1 = [101, 102, 103]
        target = 0.03954295145631068
        result = Fee(100).estimate_fees(allocation, hlc_0, hlc_1)
        self.assertEquals(target, result)
